<?php

/**
 * The WeFoster Dashboard Plugin
 *
 * @package WeFoster Dashboard
 * @subpackage Main
 */

/**
 * Plugin Name: WeFoster Dashboard
 * Description: Manage your WeFoster account and purchases right from your admin.
 * Plugin URI:  https://github.com/wefoster/wefoster-dashboard/
 * Version:     1.0.0
 * Author:      WeFoster
 * Author URI:  https://www.wefoster.co/
 * Text Domain: wefoster
 * Domain Path: /languages/
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WeFoster' ) ) :
/**
 * The main plugin class
 *
 * @since 1.0.0
 */
final class WeFoster {

	/**
	 * Setup and return the singleton pattern
	 *
	 * @since 1.0.0
	 *
	 * @uses WeFoster::setup_globals()
	 * @uses WeFoster::setup_actions()
	 * @return The single WeFoster
	 */
	public static function instance() {

		// Store instance locally
		static $instance = null;

		if ( null === $instance ) {
			$instance = new WeFoster;
			$instance->setup_globals();
			$instance->includes();
			$instance->setup_actions();
		}

		return $instance;
	}

	/**
	 * Prevent the plugin class from being loaded more than once
	 */
	private function __construct() { /* Nothing to do */ }

	/** Private methods *************************************************/

	/**
	 * Setup default class globals
	 *
	 * @since 1.0.0
	 */
	private function setup_globals() {

		/** Versions **********************************************************/

		$this->version      = '1.0.0';

		/** Paths *************************************************************/

		// Setup some base path and URL information
		$this->file         = __FILE__;
		$this->basename     = plugin_basename( $this->file );
		$this->plugin_dir   = plugin_dir_path( $this->file );
		$this->plugin_url   = plugin_dir_url ( $this->file );

		// Includes
		$this->includes_dir = trailingslashit( $this->plugin_dir . 'includes' );
		$this->includes_url = trailingslashit( $this->plugin_url . 'includes' );

		// Languages
		$this->lang_dir     = trailingslashit( $this->plugin_dir . 'languages' );

		/** Misc **************************************************************/

		$this->extend       = new stdClass();
		$this->domain       = 'wefoster';
	}

	/**
	 * Include the required files
	 *
	 * @since 1.0.0
	 */
	private function includes() {

		/** Vendors *******************************************************/

		require( $this->includes_dir . 'vendor/acf.php' );

		/** Admin *********************************************************/

		// When in admin, do as the admins do
		if ( is_admin() ) {
			require( $this->includes_dir . 'pages.php' );
		}

		/** Hooks *********************************************************/
		
		require( $this->includes_dir . 'actions.php' );
	}

	/**
	 * Setup default actions and filters
	 *
	 * @since 1.0.0
	 */
	private function setup_actions() {

		// Load textdomain
		add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );

		// And... we're live!
		do_action( 'wefoster_loaded' );
	}

	/** Plugin **********************************************************/

	/**
	 * Load the translation file for current language. Checks the languages
	 * folder inside the plugin first, and then the default WordPress
	 * languages folder.
	 *
	 * Note that custom translation files inside the plugin folder will be
	 * removed on plugin updates. If you're creating custom translation
	 * files, please use the global language folder.
	 *
	 * @since 1.0.0
	 *
	 * @uses apply_filters() Calls 'plugin_locale' with {@link get_locale()} value
	 * @uses load_textdomain() To load the textdomain
	 * @uses load_plugin_textdomain() To load the textdomain
	 */
	public function load_textdomain() {

		// Traditional WordPress plugin locale filter
		$locale        = apply_filters( 'plugin_locale', get_locale(), $this->domain );
		$mofile        = sprintf( '%1$s-%2$s.mo', $this->domain, $locale );

		// Setup paths to current locale file
		$mofile_local  = $this->lang_dir . $mofile;
		$mofile_global = WP_LANG_DIR . '/wefoster/' . $mofile;

		// Look in global /wp-content/languages/wefoster folder
		load_textdomain( $this->domain, $mofile_global );

		// Look in local /wp-content/plugins/wefoster/languages/ folder
		load_textdomain( $this->domain, $mofile_local );

		// Look in global /wp-content/languages/plugins/
		load_plugin_textdomain( $this->domain );
	}

	/** Public methods **************************************************/

	// Do stuff
}

/**
 * Return single instance of this main plugin class
 *
 * @since 1.0.0
 *
 * @return WeFoster
 */
function wefoster() {
	return WeFoster::instance();
}

// Initiate
wefoster();

endif; // class_exists
