=== WeFoster Dashboard ===
Contributors: WeFoster
Tags: buddypress, wefoster, dashboard
Requires at least: 4
Tested up to: 4.2.1
Stable tag: 1.0.10
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

The Dashboard Plugin for WeFoster

== Description ==

Coming Soon

== Installation ==

1. Install the WeFoster Dashboard plugin in `wp-content/plugins/` and activate at Dashboard > Plugins
1. Follow the on-screen guide to get your community set up in just minutes

== Frequently Asked Questions ==

= Where do I get support? =

If you have any problems please get in touch with us through the forums.

== Screenshots ==



== Translation credits ==


== Changelog ==



== Credits ==

Proudly developed by the WeFoster team.
