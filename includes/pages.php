<?php

/**
 * WeFoster Dashboard Pages
 *
 * @package WeFoster Dashboard
 * @subpackage Pages
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WeFoster_Dashboard_Pages' ) ) :
/**
 * The WeFoster Dashboard Pages class
 *
 * @since 1.0.0
 */
class WeFoster_Dashboard_Pages {

	/**
	 * Holds the collection of page details
	 *
	 * @since 1.0.0
	 * @var array
	 */
	protected $pages = array();

	/**
	 * Class constructor
	 *
	 * @since 1.0.0
	 *
	 * @uses is_admin()
	 * @uses WeFoster_Dashboard_Pages::setup_actions()
	 */
	public function __construct() {

		// Only run when in the admin
		if ( ! is_admin() )
			return;

		$this->includes();
		$this->setup_actions();
	}

	/**
	 * Include required files
	 *
	 * @since 1.0.0
	 */
	private function includes() {
		require_once( wefoster()->includes_dir . 'pages/class-wefoster-dashboard-page.php' );
	}

	/**
	 * Setup class hooks and filters
	 *
	 * @since 1.0.0
	 */
	private function setup_actions() {

		// Register pages
		add_action( 'wefoster_loaded', array( $this, 'register_default_pages' ) );

		// Admin
		add_action( 'admin_menu',            array( $this, 'admin_menus'      ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts'  ) );
		add_action( 'admin_body_class',      array( $this, 'admin_body_class' ) );
	}

	/** Page Registration *************************************************/

	/**
	 * Register the default dashboard pages
	 *
	 * @since 1.0.0
	 *
	 * @uses wefoster()
	 * @uses WeFoster_Dashboard_Pages::register_page()
	 */
	public function register_default_pages() {

		// Get WeFoster
		$wf = wefoster();

		// Dashboard Home
		$this->register_page( array(
			'title'      => __( 'WeFoster Dashboard', 'wefoster' ),
			'slug'       => 'wefoster-dashboard',
			'parent'     => false,
			'labels'     => array(
				'menu'    => __( 'Your Dashboard',                  'wefoster' ),
				'loading' => __( 'Loading your WeFoster Dashboard', 'wefoster' )
			),
			'priority'   => 0,
			'icon'       => $wf->includes_url . 'assets/img/logo-dashboard.png',
			'load_icon'  => $wf->includes_url . 'assets/img/logo-loading.png',
			'iframe_src' => 'https://wefoster.co/wefoster-dashboard-login/',
			'_builtin'   => true,
		) );

		// Community News
		$this->register_page( array(
			'title'      => __( 'Community News', 'wefoster' ),
			'slug'       => 'wefoster-community-news',
			'priority'   => 5,
			'icon'       => $wf->includes_url . 'assets/img/logo-dashboard.png',
			'load_icon'  => $wf->includes_url . 'assets/img/logo-loading.png',
			'iframe_src' => 'https://wefoster.co/buddypress-news/',
			'_builtin'   => true,
		) );

		/**
		 * Some pages have more body to them, so they're having their
		 * own class. You can find them in the includes/pages folder.
		 */
		foreach ( array(
			'WeFoster_Licenses_Page' // Licenses page
		) as $page_class ) {
			$page_file = $wf->includes_dir . 'pages/class-' . strtolower( str_replace( '_', '-', $page_class ) ) . '.php';

			// Load page class file
			if ( file_exists( $page_file ) ) {
				require_once( $page_file );

				// Declare new page
				if ( class_exists( $page_class ) ) {
					new $page_class();
				}
			}
		}
	}

	/**
	 * Register a dashboard page
	 *
	 * @since 1.0.0
	 *
	 * @uses WeFoster_Dashboard_Pages::setup_page_labels()
	 *
	 * @param array $args Page parameters
	 * @return slug|bool The registered page slug or False when not successful.
	 */
	public function register_page( $args = array() ) {

		// Merge with defaults
		$args = wp_parse_args( $args, array(
			'title'       => '',
			'slug'        => '',
			'parent'      => 'wefoster-dashboard',
			'capability'  => 'manage_options',
			'labels'      => array(),
			'priority'    => 10,
			'icon'        => '',
			'load_icon'	  => '',
			'iframe_src'  => '',
			'callback'    => '',
			'acf'         => false,
			'_builtin'    => false
		) );

		// Bail when we're missing important arguments
		if ( empty( $args['title'] ) || empty( $args['slug'] ) )
			return false;

		// Bail when overwriting pages
		if ( isset( $this->pages[ $args['slug'] ] ) )
			return false;

		// Define page labels
		$args['labels'] = $this->setup_page_labels( $args );

		// Set iframe page callback
		if ( ! empty( $args['iframe_src'] ) && empty( $args['callback'] ) ) {
			$args['callback'] = array( $this, 'render_iframe_page' );
		}

		// TODO: sanitize args
		$page = (object) $args;

		// Register this page
		$this->pages[ $page->slug ] = $page;

		return $page->slug;
	}

	/**
	 * Update a page variable
	 *
	 * @since 1.0.0
	 *
	 * @param string $slug Unique page slug
	 * @param string $arg Page variable name
	 * @param mixed $value New variable value
	 * @return bool Update success
	 */
	public function set_page_var( $slug, $arg, $value ) {

		// Bail when the page is not found
		if ( ! isset( $this->pages[ $slug ] ) )
			return false;

		// Set page variable
		// TODO: sanitize args
		$this->pages[ $slug ]->{$arg} = $value;

		return true;
	}

	/**
	 * Setup and return the page labels
	 *
	 * @since 1.0.0
	 *
	 * @param array $args Page registration parameters
	 * @return array Page labels
	 */
	private function setup_page_labels( $args ) {

		// Get the page title
		$title = $args['title'];

		// Setup default labels
		$labels = array(
			'menu'    => $title,
			'loading' => sprintf( __( 'Loading %s', 'wefoster' ), $title ),
		);

		// Merge labels
		if ( is_array( $args['labels'] ) ) {
			$labels = wp_parse_args( $args['labels'], $labels );
		}

		return $labels;
	}

	/**
	 * Return (a filtered selection of) the registered pages
	 *
	 * @since 1.0.0
	 *
	 * @param array $args Filter arguments. See {@link wp_list_filter()}.
	 * @param array $operator Filter operator. See {@link wp_list_filter()}.
	 * @return array Pages
	 */
	public function get_pages( $args = array(), $operator = 'AND' ) {
		return wp_list_filter( $this->pages, $args, $operator );
	}

	/**
	 * Register admin menus
	 *
	 * @since 1.0.0
	 *
	 * @uses WeFoster_Dashboard_Pages::get_pages()
	 * @uses add_menu_page()
	 * @uses acf_add_options_sub_page()
	 * @uses add_submenu_page()
	 */
	public function admin_menus() {

		// Register page menus
		foreach ( $this->get_pages() as $page ) {

			// This is a top level page
			if ( ! empty( $page->parent ) ) {
				add_menu_page( $page->title, $page->labels['menu'], $page->capability, $page->slug, $page->callback, $page->icon, $page->priority );

			// This is a sub level page
			} else {

				// ACF options page
				if ( true === $page->acf ) {

					// Define ACF menu title
					$page->menu_title = $page->labels['menu'];

					// Register ACF options page
					acf_add_options_sub_page( (array) $page );

				// Default sub page
				} else {
					add_submenu_page( $page->parent, $page->title, $page->labels['menu'], $page->capability, $page->slug, $page->callback, $page->icon, $page->priority );
				}
			}
		}
	}

	/**
	 * Enqueue admin scripts
	 *
	 * @since 1.0.0
	 *
	 * @uses wp_register_style()
	 * @uses wp_enqueue_script()
	 * @uses wp_enqueue_style()
	 */
	public function enqueue_scripts() {
		$wf = wefoster();

		// Register styles
		wp_register_style( 'wefoster-dashboard-style', $wf->includes_url . 'assets/css/style.css' );

		// Register a tiny stylesheet for our menus. Inline CSS is to messy.
		wp_register_style( 'wefoster-menu-style', $wf->includes_url . 'assets/css/menu.css' );
		wp_enqueue_style( 'wefoster-menu-style' );
		wp_add_inline_style( 'wefoster-menu-style', $this->inline_style() );

		// Bail when this is not one of our dashboard pages
		if ( ! isset( $_GET['page'] ) || ! in_array( $_GET['page'], wp_list_pluck( $this->get_pages(), 'slug' ) ) )
			return;

		wp_enqueue_script( 'iframe-resizer', $wf->includes_url . 'assets/js/iframeResizer.min.js' );
		wp_enqueue_script( 'wefoster-dashboard-custom', $wf->includes_url . 'assets/js/custom.js' );

		wp_enqueue_style( 'wefoster-dashboard-style' );
	}

	/**
	 * Return inline styles to accompany the admin CSS.
	 *
	 * @since 1.0.0
	 */
	public function inline_style() {
		$css = '';

		/**
		 * Allow developers to hide the WeFoster Dashboard. The functionality and 
		 * license verification is still required, hence the use of CSS.
		 */
		if ( defined( 'WEFOSTER_HIDE_DASHBOARD' ) && WEFOSTER_HIDE_DASHBOARD ) {
			$css .= '.toplevel_page_wefoster-dashboard { display: none; }';
		}

		return $css;
	}

	/**
	 * Filter admin body classes
	 *
	 * @since 1.0.0
	 *
	 * @param string $class Space-separated class names
	 * @return string Class names
	 */
	public function admin_body_class( $class ) {

		// Add a class for our iframe pages
		if ( isset( $_GET['page'] ) && in_array( $_GET['page'], wp_list_pluck( $this->get_pages( array( 'iframe_src' => '' ), 'NOT' ), 'slug' ) ) ) {
			$class .= ' wefoster-dashboard-iframe';
		}

		return $class;
	}

	/**
	 * Display the dasbhoard iframe page
	 *
	 * @since 1.0.0
	 */
	public function render_iframe_page() {

		// Bail when we don't know the page
		if ( ! isset( $_GET['page'] ) || ! isset( $this->pages[ $_GET['page'] ] ) )
			return;

		// Get the page details
		$page = $this->pages[ $_GET['page'] ];

		// Bail when the iframe source is missing
		if ( empty( $page->iframe_src ) )
			return;

		// Define local variable(s)
		$iframe_src = add_query_arg( array( 'class' => 'wefoster-dashboard' ), $page->iframe_src );

		// Output the loader and iframe HTML ?>

		<div class="pre-loader show-loader fade-in one loading-message">
			<div class="pre-loader-message">
				<div class="drawing" id="loading">
					<img src="<?php echo esc_url( $page->load_icon ); ?>">
					<div class="the-loading-message">
						<h3><?php echo esc_html( $page->labels['loading'] ); ?></h3>
					</div>
				<div class="loading-dot"></div>
				</div>
			</div>
		</div><!-- .pre-loader -->

		<div class="iframe-wrapper">
			<iframe id="wefoster-dashboard" class="pre-loader show-loader" src="<?php echo esc_url( $iframe_src ); ?>" width="100%" height="100%" scrolling="no"></iframe>
		</div>

		<script>
			// Resize iframe to fit in window width
			iFrameResize();
		</script>

		<?php
	}
}

/**
 * Setup WeFoster Dashboard pages
 *
 * @since 1.0.0
 *
 * @uses wefoster()
 */
function wefoster_dashboard_pages() {
	wefoster()->pages = new WeFoster_Dashboard_Pages;
}

endif; // class_exists
