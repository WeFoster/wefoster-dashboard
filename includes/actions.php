<?php

/**
 * WeFoster Dashboard Actions & Filters
 * 
 * @package WeFoster Dasboard
 * @subpackage Hooks
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/** Vendors ***************************************************************/

add_action( 'wefoster_loaded', 'wefoster_acf', 2 );

/** Plugin ****************************************************************/

add_action( 'init', 'wefoster_init' );

/** Dashboard *************************************************************/

if ( is_admin() ) {
	add_action( 'wefoster_loaded', 'wefoster_dashboard_pages', 5 );
}

/** Sub-actions ***********************************************************/

/**
 * Register plugin init hook
 *
 * @since 1.0.0
 *
 * @uses do_action() Calls 'wefoster_init'
 */
function wefoster_init() {
	do_action( 'wefoster_init' );
}
