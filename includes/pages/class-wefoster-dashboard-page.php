<?php

/**
 * The WeFoster Dashboard Page Base class
 * 
 * @package WeFoster Dashboard
 * @subpackage Pages
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WeFoster_Dashboard_Page' ) ) :
/**
 * The WeFoster Licenses class
 *
 * @since 1.0.0
 */
abstract class WeFoster_Dashboard_Page {

	/**
	 * Page constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct( $args = array() ) {

		// Merge with defaults
		$this->args = wp_parse_args( $args, array( 
			'title'       => '',
			'slug'        => '',
			'parent'      => 'wefoster-dashboard',
			'capability'  => 'manage_options',
			'labels'      => array(),
			'priority'    => 10,
			'icon'        => '',
			'iframe_src'  => '',
			'callback'    => '',
			'acf'         => false,
			'_builtin'    => false
		) );

		$this->setup_actions();
	}

	/**
	 * Magic getter for returning page args
	 *
	 * @since 1.0.0
	 */
	public function __get( $key ) {
		if ( in_array( $key, array_keys( $this->args ) ) ) { 
			return $this->args[ $key ];
		} else {
			return $this->{$key};
		}
	}

	/**
	 * Register page actions
	 *
	 * @since 1.0.0
	 */
	protected function setup_actions() {

		// Register page
		add_action( 'wefoster_loaded', array( $this, 'register_page' ), 20 );

		// Register ACF fields
		if ( $this->acf ) {
			add_action( 'wefoster_init', array( $this, 'register_fields' ) );
		}
	}

	/**
	 * Register this page
	 *
	 * @since 1.0.0
	 *
	 * @uses WeFoster_Dashboard_Pages::register_page()
	 */
	public function register_page() {

		// Get Pages interface
		$pages = wefoster()->pages;

		// Register page
		$pages->register_page( $this->args );

		// Get the new page details
		$page = $pages->get_pages( array( 'slug' => $this->slug ) );
		if ( $page ) {
			$this->args = (array) $page[ $this->slug ];
		}
	}

	/**
	 * Register ACF fields
	 *
	 * @since 1.0.0
	 */
	public function register_fields() { 
		/* Overwrite this method in a sub class for ACF pages */ 
	}

	/**
	 * Shortcut for registering an ACF local field group on this page
	 *
	 * @since 1.0.0
	 *
	 * @uses acf_add_local_field_group()
	 * @param array $args ACF field group arguments
	 */
	public function acf_add_local_field_group( $args = array() ) {
		acf_add_local_field_group( wp_parse_args( $args, array(

			// Provide location for $this page
			'location' => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => $this->slug,
					),
				),
			)
		) ) );
	}
}

endif; // class_exists

