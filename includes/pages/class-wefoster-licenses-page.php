<?php

/**
 * WeFoster Licenses
 *
 * @package WeFoster Dashboard
 * @subpackage Licenses
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WeFoster_Licenses_Page' ) ) :
/**
 * The WeFoster Licenses class
 *
 * @since 1.0.0
 */
class WeFoster_Licenses_Page extends WeFoster_Dashboard_Page {

	/**
	 * Page constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		parent::__construct( array(
			'title' => __( 'License Management', 'wefoster' ),
			'slug'  => 'wefoster-license-management',
			// This is an ACF options page
			'acf'   => true
		) );
	}

	/**
	 * Register page actions
	 *
	 * @since 1.0.0
	 */
	public function register_fields() {

		// Set a welcome message
		$this->welcome_message();

		// License registration
		$this->product_example();

		// License sidebar
		$this->register_sidebar();
	}

	/**
	 * Start by adding a simple welcome message field.
	 *
	 * We are using ACF because this allows easily translation and potential
	 * modifications to fields for White Labeling in the future.
	 *
	 * @since 1.0.0
	 *
	 * @uses WeFoster_Licenses_Page::acf_add_local_field_group()
	 */
	public function welcome_message() {
		$this->acf_add_local_field_group( array(
			'key'    => 'wefoster_license_welcome',
			'title'  => __( 'Welcome to Your License Management', 'wefoster' ),
			'fields' => array(
				array(
					'key'     => 'wefoster_welcome_message',
					'label'   => __( 'Welcome Message', 'wefoster' ),
					'name'    => 'welcome_message',
					'type'    => 'message',
					'message' => sprintf( '<div class="wefoster-dashboard-header">%s</div>',
						__( 'Below you will find an overview of all your WeFoster products that require an active license.', 'wefoster' )
					),
				),
			),
			'menu_order' => 0,
		) );
	}

	/**
	 * This is an example of how any of our products can generate
	 * the fields for licenses.
	 *
	 * This snippet can be added to any product (theme or plugin) but I'm sure can
	 * be improved or abstracted. Additionally we still need to hook into the EDD SL
	 * plugin to verify the licens and show the license status.
	 *
	 * @since 1.0.0
	 *
	 * @uses WeFoster_Licenses_Page::acf_add_local_field_group()
	 */
	public function product_example() {
		$this->acf_add_local_field_group( array(
			'key'    => 'wefoster_license_key_product',
			'title'  => __( 'BuddyPress Group Types', 'wefoster' ),
			'fields' => array(

				// License key field
				array(
					'key'         => 'license_key_field',
					'label'       => __( 'Your License Key', 'wefoster' ),
					'name'        => 'license_key',
					'type'        => 'password',
					'placeholder' => __( 'Paste your license key', 'wefoster' ),
					'prepend'     => '<span class="dashicons dashicons-post-status"></span>',
					'wrapper'     => array(
						'width'       => '50',
						'class'       => '',
						'id'          => '',
					),
				),

				// License status field
				array(
					'key'     => 'license_status_field',
					'label'   => __( 'Status', 'wefoster' ),
					'name'    => 'license_status',
					'type'    => 'message',
					/* translators: 1. License expiration date. 2. Number of domains */
					'message' => sprintf( __( 'Your license is active until %1$s and you can activate this product on %2$s<br>', 'wefoster' ), '<strong>April 22nd, 2015</strong>', ' <strong>3 more domains</strong>' ),
					'wrapper' => array(
						'width' => '50',
						'class' => '',
						'id'    => '',
					),
				),
			),
			'menu_order' => 10,
		));
	}

	/**
	 * Register a dashboard page sidebar
	 *
	 * @since 1.0.0
	 *
	 * @uses WeFoster_Licenses_Page::acf_add_local_field_group()
	 */
	public function register_sidebar() {
		$this->acf_add_local_field_group( array(
			'key'    => 'wf_dashboard_sidebar_fields',
			'title'  => __( 'License Information', 'wefoster' ),
			'fields' => array(

				// Upgrades description
				array(
					'key'               => 'sidebar_license_upgrades',
					'label'             => __( 'License Upgrades', 'wefoster' ),
					'type'              => 'message',
					'conditional_logic' => 0,
					'message'           => __( 'Want to upgrade your license for a certain product or do you have any other questions or requests related to your purchase? You can get in touch by our customer contact form. <a href="http://wefoster.co/customer-contact" class="btn btn-primary"> Go to the customer contact form.</a>', 'wefoster' ),
				),

				// Licenses FAQ
				array(
					'key'     => 'sidebar_commons_questions',
					'label'   => __( 'Common Questions', 'wefoster' ),
					'type'    => 'message',
					'message' => '
						<ul>
							<li><a href="http://wefoster.co/questions/can-i-update-my-license-later-if-i-need-this-product-on-more-domains/" class="btn btn-primary">' . __( 'Can I upgrade my license?', 'wefoster' ) . '</a></li>
							<li><a href="http://wefoster.co/questions/can-i-update-my-license-later-if-i-need-this-product-on-more-domains/" class="btn btn-primary">' . __( 'Can I still use my product when my license expires?', 'wefoster' ) . '</a></li>
						</ul>',
				),
			),
			'menu_order' => 0,
			'position'   => 'side',
		) );
	}
}

endif; // class_exists
