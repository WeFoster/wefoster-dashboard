<?php

/**
 * WeFoster Advanced Custom Fields
 *
 * @since 1.0.0
 *
 * @package WeFoster Dashboard
 * @subpackage ACF
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WeFoster_ACF' ) ) :
/**
 * WeFoster ACF setup
 *
 * @since 1.0.0
 */
class WeFoster_ACF {

	/**
	 * Class constructor
	 *
	 * @since 1.0.0
	 *
	 * @uses WeFoster_ACF::includes()
	 * @uses WeFoster_ACF::setup_actions()
	 */
	public function __construct() {

		// Bail when ACF is already loaded by someone else
		if ( class_exists( 'acf' ) )
			return;

		$this->includes();
		$this->setup_actions();
	}

	/**
	 * Include the reuqired files
	 *
	 * @since 1.0.0
	 */
	private function includes() {
		include_once( wefoster()->includes_dir . '/vendor/acf/acf.php' );
	}

	/**
	 * Setup default hooks and filters
	 *
	 * @since 1.0.0
	 *
	 * @uses add_filter()
	 */
	private function setup_actions() {
		add_filter( 'acf/settings/path', array( $this, 'acf_settings_path' ) );
		add_filter( 'acf/settings/dir',  array( $this, 'acf_settings_url'  ) );

		// Hide ACF field group menu item
		//add_filter( 'acf/settings/show_admin', '__return_false' );
	}

	/**
	 * Return the path to the location of ACF
	 *
	 * @since 1.0.0
	 * 
	 * @param string $path ACF path location
	 * @return string Path
	 */
	public function acf_settings_path( $path ) {
		return trailingslashit( wefoster()->includes_dir . 'vendor/acf' );
	}

	/**
	 * Return the url to the location of ACF
	 *
	 * @since 1.0.0
	 * 
	 * @param string $url ACF url location
	 * @return string Url
	 */
	public function acf_settings_url( $url ) {
		return trailingslashit( wefoster()->includes_url . 'vendor/acf' );
	}
}

/**
 * Register ACF with this plugin
 *
 * @since 1.0.0
 *
 * @uses wefoster()
 */
function wefoster_acf() {
	wefoster()->acf = new WeFoster_ACF;
}

endif; // class_exists
